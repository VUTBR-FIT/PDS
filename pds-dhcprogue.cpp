/*
 * File:   pds-dhcprogue.cpp
 * Author: Lukas Cerny (xcerny63)
 */

#include <cstdlib>
#include <iterator>
#include <mutex>
#include <signal.h>
#include <sstream>
#include <string>
#include <unistd.h>
#include <vector>

#include <pcap.h>
#include <thread>
#include <cstring>
#include <bitset>

#include "basic.h"
#include "debug.h"
#include "dhcp.h"
#include "udp.h"
#include "ip.h"
#include "ethernet.h"

#define DHCP_RESERVATION_WATING_TIME 100

using namespace std;
/** Stavy pro vlakna a pro rezervovane IP */
enum resource_status {
    ACTIVE,
    WAITING,
    NOTACTIVE,
    RESERVED
};
/**
 * Hodnoty spracovanych vstupnich parametr
 * a vygenerovanych hodnot pro DHCP serve
 */
struct params {
    string interface;
    uint8_t pool_start[4];
    uint8_t pool_end[4];
    uint8_t gateway[4];
    uint8_t dns_server[4];
    string domain;
    unsigned int lease_time;
    uint8_t netmask[4];
    uint8_t mac_addres[6];
    uint8_t dhcp[4];
};
/** IP adresa namapovana na MAC adres
 *  a casem rezervace a stavem rezervace */
struct mapping {
    uint32_t ip;
    uint8_t mac[6];
    time_t added_at;
    resource_status state;
};
// adresovy prostor DHCP serveru
vector<struct mapping> dhcpResources;
// hodnoty zopracovanych parametru
struct params params;
// prijate pakety
vector<uint8_t *> packets;
// Synchronizace vlaken pro pristup k prijatym paketum
mutex lockPackets;
mutex lockPacketsPre;
mutex waitPacket;

#define MAX_THREAD 10 // pocet spoustenych vlaken
/**
 * Inicializace komunikace a nastaveni filtr
 * @param params Zpracovane vstupni paramet
 * @return Uspech inicializace
 */
int initComunication(struct params params);
/**
 * Vygeneruje adresni rozsah DHCP server
 * @param start Prvni adresa z rozsah
 * @param end   Posledni adresa rozsah
 */
void initIPAddress(uint8_t *start, uint8_t *end);
/**
 * Vyhleda rezervovanou IP adresu podle zadane MAC adres
 * @param mac MAC adresa, podle ktere se bude vhledava
 * @return Nalezena IP adres
 */
uint32_t getIpByMac(uint8_t *mac);
/**
 * Z adresniho prostoru vybere volnou IP adres
 * @param result    Vybrana IP adres
 * @param leaseTime Doba, po kterou adresa bude rezervovan
 * @return Uspech rezervace. Pokud adresni prostor neobsahuje volnou IP adresu, tak vraci false
 */
bool getIPAddress(uint32_t *result, unsigned int leaseTime);
/**
 * Zarezervuje danou IP adresu a nastavi k ni mac adresu, dobu platnosti a typ rezervac
 * @param ip      IP adresa,ktere se bude rezervova
 * @param mac     MAC adresa, ktere je IP adresa pridelovan
 * @param status  Druh rezervac
 */
void reserveDhcpResource(uint32_t ip, uint8_t *mac, resource_status status);
/**
 * Ziska masku site z adresniho rozsahu DHCP server
 * @param start Prvni adresa z rozsah
 * @param end   Posledni adresa z rozsah
 * @param mask  Ziskana maska sit
 */
void getNetworkMask(uint8_t *start, uint8_t *end, uint8_t *mask);
/**
 * Rozdeli vstupni text a rozdeli ho podle oddelovace
 * a vysledek operace vraci jako vektor retezcu
 * @param source    Text k rozdeleni
 * @param separator Oddelovac, podle ktereho se vstupni text del
 * @return Rozdeleny tex
 */
vector<string> split(string source, char separator);
/**
 * Funkce reprezentujici cinnost vlakna pro zpracovani prijatych pozadavk
 * @param params Zoracovane vstupni parametr
 */
void requestHandler(struct params params);
/**
 * Funkce reprezentujici cinnost vlakna pro prijem paket
 */
void recvPackets();
/**
 * Vypise cely adresovy prostor a prirazene MAC adresy a stav rezervac
 */
void printDHCPResources() {
    uint8_t tempIp[4];
    for (struct mapping temp : dhcpResources) {
        memcpy(tempIp, &(temp.ip), 4 * sizeof (uint8_t));
        fprintf(stderr, "IP %d.%d.%d.%d\t", tempIp[0], tempIp[1], tempIp[2], tempIp[3]);
        if (temp.mac != NULL)
            fprintf(stderr, "MAC %02x:%02x:%02x:%02x:%02x:%02x\t", temp.mac[0], temp.mac[1], temp.mac[2], temp.mac[3], temp.mac[4], temp.mac[5]);
        switch (temp.state) {
            case ACTIVE: fprintf(stdout, "%s", "ACTIVE\n");
                break;
            case NOTACTIVE: fprintf(stdout, "%s", "NOACTIVE\n");
                break;
            case RESERVED: fprintf(stdout, "%s", "RESERVED\n");
                break;
            default: fprintf(stdout, "%s", "WAITING\n");
                break;
        }
    }
}

/*
 *
 */
int main(int argc, char** argv) {
    signal(SIGINT, signalHandler);

    thread requestHandlers[MAX_THREAD];
    int code;
    int optVal;
    vector<string> temp, temp_start, temp_end;
    bool i = false,
            p = false,
            g = false,
            n = false,
            d = false,
            l = false;
    if (argc != 13) {
        fprintf(stderr, "%s\n", "Bad arguments");
        return E_BAD_ARGUMENT;
    }
    // Zpracovani vstupnich parametru
    while ((optVal = getopt(argc, argv, "i:p:g:n:d:l:")) != -1) {
        temp.clear();
        switch (optVal) {
            case 'i':
                if (i == true) {
                    fprintf(stderr, "Duplicated parameter '%s'\n", "i");
                    return E_BAD_ARGUMENT;
                }
                i = true;
                params.interface = optarg;
                break;
            case 'p':
                if (p == true) {
                    fprintf(stderr, "Duplicated parameter '%s'\n", "p");
                    return E_BAD_ARGUMENT;
                }
                p = true;
                temp = split(optarg, '-');
                if (temp.size() != 2) {
                    fprintf(stderr, "Bad input format -p %s\n", optarg);
                    return E_BAD_ARGUMENT;
                }
                temp_start = split(temp[0], '.');
                temp_end = split(temp[1], '.');
                if (temp_start.size() != 4 || temp_end.size() != 4) {
                    fprintf(stderr, "Bad input IP format -p %s\n", optarg);
                    return E_BAD_ARGUMENT;
                }
                params.pool_start[0] = (uint8_t) stoi(temp_start[0].c_str());
                params.pool_start[1] = (uint8_t) stoi(temp_start[1].c_str());
                params.pool_start[2] = (uint8_t) stoi(temp_start[2].c_str());
                params.pool_start[3] = (uint8_t) stoi(temp_start[3].c_str());
                params.pool_end[0] = (uint8_t) stoi(temp_end[0].c_str());
                params.pool_end[1] = (uint8_t) stoi(temp_end[1].c_str());
                params.pool_end[2] = (uint8_t) stoi(temp_end[2].c_str());
                params.pool_end[3] = (uint8_t) stoi(temp_end[3].c_str());
                break;
            case 'g':
                if (g == true) {
                    fprintf(stderr, "Duplicated parameter '%s'\n", "g");
                    return E_BAD_ARGUMENT;
                }
                temp = split(optarg, '.');
                if (temp.size() != 4) {
                    fprintf(stderr, "Bad input format -g %s\n", optarg);
                    return E_BAD_ARGUMENT;
                }
                g = true;
                params.gateway[0] = (uint8_t) stoi(temp[0].c_str());
                params.gateway[1] = (uint8_t) stoi(temp[1].c_str());
                params.gateway[2] = (uint8_t) stoi(temp[2].c_str());
                params.gateway[3] = (uint8_t) stoi(temp[3].c_str());
                break;
            case 'n':
                if (n == true) {
                    fprintf(stderr, "Duplicated parameter '%s'\n", "n");
                    return E_BAD_ARGUMENT;
                }
                n = true;
                temp = split(optarg, '.');
                if (temp.size() != 4) {
                    fprintf(stderr, "Bad input format -n %s\n", optarg);
                    return E_BAD_ARGUMENT;
                }
                params.dns_server[0] = (uint8_t) stoi(temp[0].c_str());
                params.dns_server[1] = (uint8_t) stoi(temp[1].c_str());
                params.dns_server[2] = (uint8_t) stoi(temp[2].c_str());
                params.dns_server[3] = (uint8_t) stoi(temp[3].c_str());
                break;
            case 'd':
                if (d == true) {
                    fprintf(stderr, "Duplicated parameter '%s'\n", "d");
                    return E_BAD_ARGUMENT;
                }
                d = true;
                params.domain = optarg;
                break;
            case 'l':
                if (l == true) {
                    fprintf(stderr, "Duplicated parameter '%s'\n", "l");
                    return E_BAD_ARGUMENT;
                }
                l = true;
                params.lease_time = (unsigned int) std::stoul(optarg, NULL, 10);
                DEBUG_PRINT("[%s]\t LEASE_TIME %d\n", "MAIN", params.lease_time);
                break;
        }
    }

    if ((code = initComunication(params)) != E_OK)
        return code;
    initIPAddress(params.pool_start, params.pool_end);
    uint32_t temp32 = 0;
    memcpy(&temp32, &(params.dns_server), sizeof (uint32_t));
    reserveDhcpResource(temp32, NULL, RESERVED);                                // Rezervace IP adresy pro DNS server
    memcpy(&temp32, &(params.gateway), sizeof (uint32_t));
    reserveDhcpResource(temp32, NULL, RESERVED);                                // Rezervace IP adresy pro vychozi branu
    uint8_t *mac = generateMac();                                               // Vygenerovani MAC adresy DHCP serveru
    memcpy(params.mac_addres, mac, 6 * sizeof (uint8_t));                       // Nastaveni MAC adresy DHCP serveru
    getIPAddress(&temp32, 0);                                                   // Ziskani IP adresy DHCP serveru
    memcpy(params.dhcp, &temp32, 4 * sizeof (uint8_t));             
    reserveDhcpResource(temp32, NULL, RESERVED);                                // Rezervace IP adresy DHCP serveru
    getNetworkMask(params.pool_start, params.pool_end, params.netmask);
#ifdef DEBUG_LOG_ENABLED
    printDHCPResources();
#endif
    DEBUG_PRINT("[%s]\t FINAL MASK %d.%d.%d.%d\n", "MAIN", params.netmask[0], params.netmask[1], params.netmask[2], params.netmask[3]);
    stateProgram = CONTINUE;

    waitPacket.lock();

    for (int i = 0; i < MAX_THREAD; i++)
        requestHandlers[i] = thread(requestHandler, params);

    DEBUG_LOG("MAIN", "PRE_RECV");
    thread recv(recvPackets);
    recv.join();
    DEBUG_LOG("MAIN", "RECV");
    for (auto &temp : requestHandlers) {
        temp.join();
    }

    return E_OK;
}

void signalHandler(int signal) {
    stateProgram = STOP;
    pcap_close(sendHandle);
    pcap_close(recvHandle);
    exit(E_OK);
}

void initIPAddress(uint8_t *start, uint8_t *end) {
    DEBUG_PRINT("[%s]\t START IP: %d.%d.%d.%d\n", "INIT_IP_ADDR", start[0], start[1], start[2], start[3]);
    DEBUG_PRINT("[%s]\t END IP: %d.%d.%d.%d\n", "INIT_IP_ADDR", end[0], end[1], end[2], end[3]);
    uint32_t start32, end32;
    uint32_t temp32 = 0;
    memcpy(&temp32, start, sizeof (uint32_t));
    start32 = htonl(temp32);
    temp32 = 0;
    memcpy(&temp32, end, sizeof (uint32_t));
    end32 = htonl(temp32);

    for (uint32_t i = start32; i < end32; i++) {
        struct mapping temp;
        temp.ip = ntohl(i);
        temp.state = NOTACTIVE;
        temp.added_at = 0;
        memset(temp.mac, 0, 6 * sizeof (uint8_t));
        dhcpResources.push_back(temp);
    }
    DEBUG_PRINT("[%s]\t COUNT IPs: %d\n", "INIT_IP_ADDR", temp);
}

void reserveDhcpResource(uint32_t ip, uint8_t *mac, resource_status status) {
#ifdef DEBUG_LOG_ENABLE
    uint8_t tempIp[4];
    memcpy(&tempIp, &ip, 4 * sizeof (uint8_t));
    if (mac != NULL)
        fprintf(stdout, "IP: %d.%d.%d.%d, MAC: %02x:%02x:%02x:%02x:%02x:%02x", tempIp[0], tempIp[1], tempIp[2], tempIp[3], mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    else fprintf(stdout, "IP: %d.%d.%d.%d", tempIp[0], tempIp[1], tempIp[2], tempIp[3]);
    switch (status) {
        case WAITING: fprintf(stdout, " %s\n", "WAITIONG");
            break;
        case RESERVED: fprintf(stdout, " %s\n", "RESERVED");
            break;
        default:fprintf(stdout, " %s\n", "");
            break;
    }
#endif
    for (struct mapping &temp : dhcpResources) {
        //if (temp[0] == ip[0] && temp[1] == ip[1] && temp[2] == ip[2] && temp[3] == ip[3]) {
        if (status != WAITING && status != RESERVED) {
            if (temp.mac[0] == mac[0] && temp.mac[1] == mac[1] && temp.mac[2] == mac[2] && temp.mac[3] == mac[3] && temp.mac[4] == mac[4] && temp.mac[5] == mac[5]) {
                temp.state = status;
                if (status == NOTACTIVE)
                    memset(temp.mac, 0, 6 * sizeof (uint8_t));
                DEBUG_LOG("RESERVED", "BY MAC");
            }
        } else if (temp.ip == ip) {
            if (status != NOTACTIVE && status != RESERVED)
                memcpy(temp.mac, mac, 6 * sizeof (uint8_t));
            temp.state = status;
            if (temp.state == WAITING)
                temp.added_at = time(NULL);

            DEBUG_LOG("RESERVED", "BY IP");


        }
    }
}

bool getIPAddress(uint32_t *result, unsigned int leaseTime) {
    for (struct mapping temp : dhcpResources) {
        if (temp.state == RESERVED) continue;
        if (temp.added_at + leaseTime < time(NULL))
            temp.state = NOTACTIVE;
        if (temp.state == NOTACTIVE) {
            *result = temp.ip;
            return true;
        }
    }
    return false;
}

void recvPackets() {
    int res;
    struct pcap_pkthdr *header;
    uint8_t *recvPacket;
    uint32_t opt32 = 0;
    const u_char *data;

    DEBUG_LOG("RECV", "START");
    while (stateProgram == CONTINUE) {
        recvPacket = (uint8_t *) malloc(BUFFER_SIZE * sizeof (uint8_t));
        //DEBUG_LOG("RECV_PACKET", "continue...");
        if ((res = pcap_next_ex(recvHandle, &header, &data)) < 0)
            continue;

        this_thread::sleep_for(chrono::milliseconds(100));

        if (res == 0) {
            //DEBUG_LOG("RECV_PACKET", "TIMEOUT");
            continue; // Cac vyprsel
        }

        if (!getIPAddress(&opt32, params.lease_time)) {
            DEBUG_LOG("REQUES_HDL", "FULL!!!!!!!");
            continue;
        };

        getDataFromPacket(data, recvPacket);

        lockPackets.lock();
        packets.push_back(recvPacket);
        waitPacket.unlock();
        lockPackets.unlock();

        DEBUG_LOG("RECV_PACKET", "ADD OK");
    }
    DEBUG_LOG("RECV", "END");
}

void requestHandler(struct params params) {
    uint8_t packet[BUFFER_SIZE], *recvPacket, option, options[4];
    uint16_t length;
    uint32_t opt32 = 0;

    struct dhcp_data *dhcp, *tempDhcp;
    struct ethhdr *ethernet, *tempEthernet;
    struct iphdr *tempIp;
    DEBUG_LOG("REQUES_HDL", "START");
    while (stateProgram == CONTINUE) {
        length = 0;
        DEBUG_LOG("REQUES_HDL", "continue");

        lockPacketsPre.lock();
        if (packets.empty())
            waitPacket.lock();
        lockPackets.lock();
        recvPacket = packets.back();
#ifdef DEBUG_LOG_ENABLED
        int tSize = packets.size();
        DEBUG_PRINT("[%s]\t SIZE_PACKETS: %d\n", "REQUES_HDL", tSize);
#endif
        packets.pop_back();
        lockPackets.unlock();
        lockPacketsPre.unlock();

        memset(packet, 0, BUFFER_SIZE);

        getOption(recvPacket, BUFFER_SIZE, DHCP_OPT_MESSAGE_TYPE, &option);
        DEBUG_PRINT("[%s]\t\t TYPE OPTION %d\n", "REQUES_HDL", option);
        if (option != DHCP_DISCOVER)
            DEBUG_LOG("REQUES_HDL", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

        dhcp = getDHCPData(recvPacket);
        ethernet = getEthernet(recvPacket);
        switch (option) {
            case DHCP_DISCOVER:
                DEBUG_LOG("REQUES_HDL", "DISCOVER");
                if (!getIPAddress(&opt32, params.lease_time)) {
                    DEBUG_LOG("REQUES_HDL", "FULL!!!!!!!");
                    continue;
                };
#ifdef DEBUG_LOG_ENABLED
                memcpy(&options, &opt32, sizeof (options));
                DEBUG_PRINT("[%s]\t NEW IP: %d.%d.%d.%d\n", "REQUES_HDL", options[0], options[1], options[2], options[3]);
#endif
                createDHCPData(packet, &length, BOOTREPLY, dhcp->chaddr, dhcp->xid);
                tempDhcp = getDHCPData(packet);

                tempDhcp->yiaddr = opt32;
                //memcpy(&(tempDhcp->siaddr), params.pool_end, sizeof (uint32_t));
                //memcpy(&(tempDhcp->yiaddr), &opt32, sizeof (uint32_t));
                memcpy(&(tempDhcp->siaddr), &(params.gateway), sizeof (uint32_t));
                option = DHCP_OFFER;
                addDHCPOption(packet, &length, DHCP_OPT_MESSAGE_TYPE, &option, sizeof (uint8_t));
                addDHCPOption(packet, &length, DHCP_OPT_MASK, params.netmask, 4 * sizeof (uint8_t));
                addDHCPOption(packet, &length, DHCP_OPT_ROUTER, params.gateway, 4 * sizeof (uint8_t));
                addDHCPOption(packet, &length, DHCP_OPT_DOMAIN_NAME_SERVER, params.dns_server, 4 * sizeof (uint8_t));
                addDHCPOption(packet, &length, DHCP_OPT_SERVER_IDENTIFIER, params.dhcp, 4 * sizeof (uint8_t));
                opt32 = params.lease_time;
                memcpy(options, &(opt32), 4 * sizeof (uint8_t));
                addDHCPOption(packet, &length, DHCP_OPT_ADDRESS_LEASE_TIME, options, sizeof (options));
                addDHCPOption(packet, &length, DHCP_OPT_MASK, params.netmask, 4 * sizeof (uint8_t));
                opt32 = params.lease_time;
                opt32 = htonl(ntohl(opt32) / 2);
                memcpy(options, &(params.lease_time), sizeof (uint32_t));
                //addDHCPOption(packet, &length, DHCP_OPT_RENEWAL, params.lease_time, sizeof (uint32_t));
                addDHCPOptionDomainName(packet, &length, params.domain);
                addDHCPOptionEnd(packet, &length);

                createUDPHeader(packet, &length, DHCP_SERVER_PORT, DHCP_CLIENT_PORT);
                createIPHeader(packet, &length);
                tempIp = getIPHeader(packet);
                tempIp->daddr = tempDhcp->yiaddr;
                tempIp->saddr = tempDhcp->siaddr;
                createEthernetHeader(packet, &length, params.mac_addres);
                tempEthernet = getEthernet(packet);
                memcpy(&(tempEthernet->h_dest), &(ethernet->h_source), sizeof (tempEthernet->h_dest));

                pcap_inject(sendHandle, (struct ethhdr *) packet, static_cast<size_t> (length));
                reserveDhcpResource(tempDhcp->yiaddr, dhcp->chaddr, WAITING);
                break;
            case DHCP_REQUEST:
                DEBUG_LOG("REQUES_HDL", "REQUEST");
                createDHCPData(packet, &length, BOOTREPLY, dhcp->chaddr, dhcp->xid);
                tempDhcp = getDHCPData(packet);
                memcpy(&(tempDhcp->siaddr), params.gateway, sizeof (uint32_t));
                tempDhcp->yiaddr = getIpByMac(dhcp->chaddr);
                option = DHCP_ACK;
                addDHCPOption(packet, &length, DHCP_OPT_MESSAGE_TYPE, &option, sizeof (uint8_t));
                addDHCPOption(packet, &length, DHCP_OPT_MASK, params.netmask, 4 * sizeof (uint8_t));
                addDHCPOption(packet, &length, DHCP_OPT_ROUTER, params.gateway, 4 * sizeof (uint8_t));
                addDHCPOption(packet, &length, DHCP_OPT_DOMAIN_NAME_SERVER, params.dns_server, 4 * sizeof (uint8_t));
                addDHCPOption(packet, &length, DHCP_OPT_SERVER_IDENTIFIER, params.dhcp, 4 * sizeof (uint8_t));
                //opt32 = params.lease_time;
                memcpy(options, &(params.lease_time), sizeof (options));
                addDHCPOption(packet, &length, DHCP_OPT_ADDRESS_LEASE_TIME, options, sizeof (options));
                addDHCPOptionDomainName(packet, &length, params.domain);
                addDHCPOptionEnd(packet, &length);

                createUDPHeader(packet, &length, DHCP_SERVER_PORT, DHCP_CLIENT_PORT);
                createIPHeader(packet, &length);
                tempIp = getIPHeader(packet);
                tempIp->daddr = tempDhcp->yiaddr;
                tempIp->saddr = tempDhcp->siaddr;
                createEthernetHeader(packet, &length, params.mac_addres);
                tempEthernet = getEthernet(packet);
                memcpy(&(tempEthernet->h_dest), &(ethernet->h_source), sizeof (tempEthernet->h_dest));

                pcap_inject(sendHandle, (struct ethhdr *) packet, static_cast<size_t> (length));

                reserveDhcpResource(tempDhcp->yiaddr, dhcp->chaddr, ACTIVE);
                break;
            case DHCP_RELEASE:
                DEBUG_LOG("REQUES_HDL", "RELEASE");
                reserveDhcpResource(dhcp->yiaddr, NULL, NOTACTIVE);
                break;
            default:
                DEBUG_LOG("REQUES_HDL", "UNKNOWN");
                break;
        }
#ifdef DEBUG_LOG_ENABLED
        printDHCPResources();
#endif
        if (recvPacket != NULL)
            free(recvPacket);
        recvPacket = NULL;
    }
}

uint32_t getIpByMac(uint8_t *mac) {

    //fprintf(stdout, "MAC: %d:%d:%d:%d:%d:%d", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    for (struct mapping temp : dhcpResources) {
        if (temp.mac[0] == mac[0] && temp.mac[1] == mac[1] && temp.mac[2] == mac[2] && temp.mac[3] == mac[3] && temp.mac[4] == mac[4] && temp.mac[5] == mac[5]) {
            DEBUG_LOG("GET_IP_BY_MAC", "FOUNDED");
            return temp.ip;
        }
    }
    DEBUG_LOG("GET_IP_BY_MAC", "NOT_FOUNDED");
    return 0;
}

void getNetworkMask(uint8_t *start, uint8_t *end, uint8_t *mask) {
    string aBin;
    string bBin;
    bool endMask = false;

    for (int i = 0; i < 4; i++) {
        aBin = bitset<8>(start[i]).to_string();
        bBin = bitset<8>(end[i]).to_string();
        for (int j = 0; j < 8; j++) {
            if (aBin[j] != bBin[j]) {
                endMask = true;
                break;
            } else {
                mask[i] = mask[i] >> 1;
                mask[i] = mask[i] | 128;
            }
        }
        DEBUG_PRINT("[%s]\t CURRENT_MASK %d.%d.%d.%d\n", "GET_NET_MASK", mask[0], mask[1], mask[2], mask[3]);
        if (endMask)
            break;
    }
    DEBUG_PRINT("[%s]\t FINAL MASK %d.%d.%d.%d\n", "GET_NET_MASK", mask[0], mask[1], mask[2], mask[3]);
}

int initComunication(struct params params) {
    char errorBuffer[PCAP_ERRBUF_SIZE];
    struct bpf_program pgm;
    bpf_u_int32 mask, net;
    string interface;
    if (pcap_lookupnet(params.interface.c_str(), &net, &mask, errorBuffer) == -1) {
        fprintf(stderr, "Could not get netmask for device %s\n", interface.c_str());
    }
    
    sendHandle = pcap_open_live(params.interface.c_str(), BUFFER_SIZE, 1, 1000, errorBuffer);
    if (sendHandle == NULL) {
        fprintf(stderr, "Could not open device %s: %s\n", interface.c_str(), errorBuffer);
        return E_LIBPCAP_FAIL;
    }
    recvHandle = pcap_open_live(params.interface.c_str(), BUFFER_SIZE, 1, -1, errorBuffer);
    if (sendHandle == NULL) {
        fprintf(stderr, "Could not open device %s: %s\n", interface.c_str(), errorBuffer);
        pcap_close(sendHandle);
        return E_LIBPCAP_FAIL;
    }
    if (pcap_compile(recvHandle, &pgm, "src port 68 and dst port 67", 0, mask) != 0) {
        fprintf(stderr, "%s\n", "pcap_compile(..,\"src port 68 and dst port 67\",..) FAIL)");
        pcap_close(sendHandle);
        pcap_close(recvHandle);
        return E_LIBPCAP_FAIL;
    }
    if (pcap_setfilter(recvHandle, &pgm) != 0) {
        fprintf(stderr, "%s\n", "pcap_setfilter() FAIL)");
        pcap_close(sendHandle);
        pcap_close(recvHandle);
        return E_LIBPCAP_FAIL;
    }
    return E_OK;
}

vector<string> split(string source, char separator) {
    vector<string> result;
    istringstream stream(source);
    string token;
    while (getline(stream, token, separator)) {
        result.push_back(token);
    }
    return result;
}
