/* 
 * File:   dhcp.h
 * Author: Lukas Cerny (xcerny63)
 */

#include <stdint.h>
#include <string>

#ifndef DHCP_H
#define DHCP_H

// Minimalni delka DHCP paketu
#define DHCP_MIN_LENGTH 300
// Definovani portu pro DHCP
#define DHCP_CLIENT_PORT 68
#define DHCP_SERVER_PORT 67

/**
 * Typ zpravy 
 */
enum DHCP_OP {
    BOOTREQUEST = 1,
    BOOTREPLY
};

/**
 * Typy DHCP zprav
 */
enum DHCP_MESSAGE_TYPE {//DHCP_OPT_MESSAGE_TYPE
    DHCP_DISCOVER = 1,
    DHCP_OFFER,
    DHCP_REQUEST,
    DHCP_DECLINE,
    DHCP_ACK,
    DHCP_NAK,
    DHCP_RELEASE
};
/**
 * Nektere vlastnosti. ktere se nastavuji v DHCP paketu
 */
enum DHCP_OPTIONS {
    DHCP_OPT_END = 0,
    DHCP_OPT_MASK,
    DHCP_OPT_TIME_OFFSET,
    DHCP_OPT_ROUTER,
    DHCP_OPT_NAME_SERVER = 5,
    DHCP_OPT_DOMAIN_NAME_SERVER,
    DHCP_OPT_DOMAIN_NAME = 15,
    DHCP_OPT_REQUEST_IP_ADDRESS = 50,
    DHCP_OPT_ADDRESS_LEASE_TIME,
    DHCP_OPT_MESSAGE_TYPE = 53,
    DHCP_OPT_SERVER_IDENTIFIER,
    DHCP_OPT_PARAMETR_REQUEST_LIST,
    DHCP_OPT_RENEWAL = 58,
    DHCP_OPT_CLIENT_IDENTIFIER = 61         
};

/**
 * Struktura pro DHCP hlavicku
 * From: https://www.ietf.org/rfc/rfc2131.txt
 * Page: 10
 */
struct dhcp_data {
    uint8_t op; // 1= Request 2=Reply
    uint8_t htype;
    uint8_t hlen;
    uint8_t hops;
    uint32_t xid;
    uint16_t secs;
    uint16_t flags;
    uint32_t ciaddr;
    uint32_t yiaddr;
    uint32_t siaddr;
    uint32_t giaddr;
    uint8_t chaddr[16];
    uint8_t sname[64];
    uint8_t file[128];
    uint8_t magic_cookie[4];
};
/**
 * Ziska pozici, na které zacina DHCP hlavikca
 * @return Pozice
 */
size_t getDHCPPrefix();
/**
 * Vrati velikost DHCP hlavicky
 * @return Velikost
 */
size_t getDHCPDataSize();
/**
 * Vytvori DHCP hlavicku a provede jeji inicializaci
 * @param packet    Paket, kam se vytvori dana hlavicka
 * @param length    Delka paketu
 * @param op        Typ zravy
 * @param mac       Mac adresa odesilatele
 * @param xid       Jednoznacny identifikator transakce
 */
void createDHCPData(uint8_t *packet, uint16_t *length, DHCP_OP op, uint8_t *mac, uint32_t xid);
/**
 * Prida do paketu dalsi DHCP vlastnost a zvetsi velikost paketu
 * @param packet        Paket, kam se vlastnost ulozi
 * @param packetLen     Delka paketu
 * @param optionType    Typ vlastnosti
 * @param optionValue   Hodnota, vkladane vlastnosti
 * @param optionSize    Velikost vkladanych dat
 */
void addDHCPOption(uint8_t *packet, uint16_t *packetLen, uint8_t optionType, uint8_t *optionValue, uint8_t optionSize);
/**
 * Prida do paketu ukoncovaci vlstnost a zkontroluje jestli paket splnuje 
 * minimalni delku DHCP paketu a popr prida bit, aby delka paketu byla suda
 * @param packet        Paket, kam se vlastnost ulozi
 * @param packetLen     Delka paketu
 */
void addDHCPOptionEnd(uint8_t *packet, uint16_t *packetLen);
/**
 * Prida do paketu mac adresu 
 * @param packet        Paket, kam se mac adresa ulozi
 * @param packetLen     Delka paketu
 * @param mac           Vkladana mac adresa
 */
void addDHCPOptionClientID(uint8_t *packet, uint16_t *packetLen, uint8_t *mac);   
/**
 * Prida do paketu domenove jmeno
 * @param packet        Paket, kam se domenove jmeno ulozi
 * @param packetLen     Delka paketu
 * @param domain        Domenove jmeno
 */
void addDHCPOptionDomainName(uint8_t *packet, uint16_t *packetLen, std::string domain);
/**
 * Vrati ukazatel na pozici v paketu, kde zacina DHCP hlavicka
 * @param packet    Zdrojovy paket
 * @return          Ukazatel na hlavicku
 */
struct dhcp_data *getDHCPData(uint8_t *packet);
/**
 * Vygeneruje a vrati nahodny identifikator transakce
 * @return Nahodny identifikator
 */
uint32_t getRandomXid();
/**
 * Ziska hodnotu DHCP vlastnoti z paketu
 * @param packet        Zdrojovy paket
 * @param packetLen     Delka paketu
 * @param optionType    Typ hledane vlastnosti
 * @param value         Nalezena hodnota
 */
void getOption(uint8_t *packet, uint16_t packetLen, uint8_t optionType, uint8_t *value);

#endif /* DHCP_H */

