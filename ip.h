/* 
 * File:   ip.h
 * Author: Lukas Cerny (xcerny63)
 */

#include <netinet/ip.h>
#include <stdint.h>
#include <string>

#ifndef IP_H
#define IP_H

/**
 * Struktura pro ulozeni MAC adresy
 */
struct mac {
    uint8_t m0;
    uint8_t m1;
    uint8_t m2;
    uint8_t m3;
    uint8_t m4;
    uint8_t m5;
};

/**
 * Vypocita velikost hlavicky jako checksum
 * Zdroj: http://www.microhowto.info/howto/calculate_an_internet_protocol_checksum_in_c.html
 * @param data
 * @param length
 * @return 
 */
uint16_t ip_checksum(void *source, size_t length);
/**
 * Ziska pozivi, na ktere zacina IP hlavicka
 * @return Pozice
 */
size_t getIPPrefix();
/**
 * Vrati velikost IP hlavicky
 * @return Velikost
 */
size_t getIPHeaderSize();
/**
 * Vytvori IP hlavicku a inicializuje ji
 * Zdroj: https://www.ietf.org/rfc/rfc791.txt
 * @param packet        Paket, kam se vytvori dana hlavicka
 * @param packetLen     Delka paketu
 */
void createIPHeader(uint8_t *packet, uint16_t *packetLen);
/**
 * Vrati ukazatel na pozici v paketu, kde zacina IP hlavicka
 * @param packet        Zdrojovy paket
 * @return              Ukazatel na hlavicku
 */
struct iphdr *getIPHeader(uint8_t *packet);

#endif /* IP_H */
