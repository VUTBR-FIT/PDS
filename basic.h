/* 
 * File:   basic.h
 * Author: Lukas Cerny (xcerny63)
 */
#include <stdlib.h>

#ifndef BASIC_H
#define BASIC_H

#ifndef BUFFER_SIZE
#define BUFFER_SIZE 4096
#endif

/**
 * Stavy programu
 */
enum status_run {
    CONTINUE,
    STOP
};

/**
 * Kody chyb
 */
enum error_code {
    E_OK = EXIT_SUCCESS,
    E_UNKNOWN = EXIT_FAILURE,
    E_BAD_ARGUMENT,
    E_LIBPCAP_FAIL
};
/**
 * Globalni promena pro sdileni stavu programu
 */
status_run stateProgram;
/**
 * Globalni promenne pro ulozeni prichozich a odchozich pripojeni
 */
pcap_t *sendHandle, *recvHandle;
/**
 * Funkce zachycujici signal SGNINT (Ctrl+C)
 * @param signal
 */
void signalHandler(int signal);

#endif /* BASIC_H */

