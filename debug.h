/* 
 * File:   debug.h
 * Author: Lukas Cerny (xcerny63)
 */

#include <stdio.h>

#ifndef DEBUG_H
#define DEBUG_H

/**
 * Debugujici zpravy
 */
#ifdef DEBUG_LOG_ENABLED
#define DEBUG_LOG(...) fprintf(stdout, "[%s]\t %s\n", __VA_ARGS__);
#define DEBUG_STAT_HEADER_SIZE(...) fprintf(stdout, "[%s]\t SUM: %d (header size: %lu)\n", __VA_ARGS__);
#define DEBUG_PRINT(...) fprintf(stdout, __VA_ARGS__);
#else
#define DEBUG_LOG(...) while (false);
#define DEBUG_STAT_HEADER_SIZE(...) while (false);
#define DEBUG_PRINT(...) while (false);
#endif

#endif /* DEBUG_H */

