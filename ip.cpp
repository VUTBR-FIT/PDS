/*
 * File:   ip.cpp
 * Author: Lukas Cerny (xcerny63)
 */

#include <arpa/inet.h>
#include <cstring>
#include <iostream>
#include <stdlib.h>
#include <vector>

#include "debug.h"
#include "ethernet.h"
#include "ip.h"
#include "dhcp.h"

uint16_t ip_checksum(void *vdata, size_t length) {
    // Cast the data pointer to one that can be indexed.
    char *data = (char *) vdata;

    // Initialise the accumulator.
    uint64_t acc = 0xffff;

    // Handle any partial block at the start og the data.
    unsigned int offset = ((uintptr_t) data)&3;
    if (offset) {
        size_t count = 4 - offset;
        if (count > length) count = length;
        uint32_t word = 0;
        memcpy(offset + (char *) &word, data, count);
        acc += ntohl(word);
        data += count;
        length -= count;
    }

    // Handle any complete 32-bit blocks.
    char * data_end = data + (length&~3);
    while (data != data_end) {
        uint32_t word;
        memcpy(&word, data, 4);
        acc += ntohl(word);
        data += 4;
    }
    length &= 3;

    // Handle any partial block at the end of the data.
    if (length) {
        uint32_t word = 0;
        memcpy(&word, data, length);
        acc += ntohl(word);
    }

    // Handle deferred carries.
    acc = (acc & 0xffffffff) + (acc >> 32);
    while (acc >> 16) {
        acc = (acc & 0xffff) + (acc >> 16);
    }

    // If the data began at an odd byte address
    // then reverse the byte order to compensate.
    if (offset & 1) {
        acc = ((acc & 0xff00) >> 8) | ((acc & 0x00ff) << 8);
    }

    // Return the checksum in network byte order.
    return htons(~acc);
}

size_t getIPPrefix() {
    return getEthernetPrefix() + getEthernetHeaderSize();
}

size_t getIPHeaderSize() {
    return sizeof (struct iphdr);
}

void createIPHeader(uint8_t *packet, uint16_t *packetLen) {
    DEBUG_LOG("GEN_IP", "Starting...");
    struct iphdr *header = getIPHeader(packet);
    header->saddr = inet_addr("0.0.0.0");
    header->id = htons(0xffff);
    header->ihl = 5; // 5 pro spravnou hlavicku
    header->protocol = IPPROTO_UDP;
    header->daddr = inet_addr("255.255.255.255"); // Broadcastova cilova adresa
    // Typ sluzby: Delay-Throughput-Reliability-Cost-MBZ
    header->tos = 16; // 16=0b10000;
    header->ttl = 242; // Maximum skoku
    header->version = IPVERSION; // IPv4
    header->tot_len = htons(static_cast<uint16_t>(*packetLen - getIPPrefix()));
    header->check = ip_checksum((unsigned short *) header, getIPHeaderSize());

#ifdef DEBUG_LOG_ENABLED
    DEBUG_STAT_HEADER_SIZE("GEN_IP", *packetLen, sizeof (struct iphdr));
    DEBUG_LOG("GEN_IP", "Finished");
#endif
}

struct iphdr *getIPHeader(uint8_t *packet) {
    return (struct iphdr *) (packet + getIPPrefix());
}
