/* 
 * File:   udp.h
 * Author: Lukas Cerny (xcerny63)
 */

#include <stdint.h>

#ifndef UDP_H
#define UDP_H
/**
 * Vrati pozici, na ktere zacina UDP hlavicka
 * @return Pozice
 */
size_t getUDPPrefix();
/**
 * Vrati velikost UPD hlavicky
 * @return Velikost
 */
size_t getUDPHeaderSize();

/**
 * Vytvori a inicializuje UDP hlavicku
 * Zdroj: https://tools.ietf.org/html/rfc768
 * @param packet        Paket, kam se vytvori dana hlavicka
 * @param packetLen     Delka paketu
 */
void createUDPHeader(uint8_t *packet, uint16_t *packetLen, int source, int dest);

#endif /* UDP_H */
