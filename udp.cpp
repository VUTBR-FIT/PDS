/* 
 * File:   udp.cpp
 * Author: Lukas Cerny (xcerny63)
 */

#include <arpa/inet.h>
#include <netinet/udp.h>

#include "debug.h"
#include "ip.h"
#include "udp.h"

#ifndef BUFFER_SIZE
#define BUFFER_SIZE 4096
#endif

size_t getUDPPrefix() {
    return getIPPrefix() + getIPHeaderSize();
}

size_t getUDPHeaderSize() {
    return sizeof (struct udphdr);
}

void createUDPHeader(uint8_t *packet, uint16_t *packetLen, int source, int dest) {
    DEBUG_LOG("GEN_UDP", "Starting...");
    struct udphdr *header = (struct udphdr *) (packet + getUDPPrefix());
    header->uh_sum = 0;
    header->uh_dport = htons(dest); 
    header->uh_sport = htons(source);
    header->uh_ulen = htons(static_cast<uint16_t>(*packetLen - getUDPPrefix()));;
#ifdef DEBUG_LOG_ENABLED
    uint16_t temp = *packetLen + getUDPHeaderSize();
    DEBUG_STAT_HEADER_SIZE("GEN_UPD", temp, sizeof(struct udphdr));
    DEBUG_LOG("GEN_UDP", "Finished");
#endif
}