CC=g++
CFLAGS=-std=c++11 -Wall -g -O3 -pthread
OUT_1=pds-dhcpstarve
OUT_2=pds-dhcprogue
SOURCE=ethernet.cpp ip.cpp udp.cpp dhcp.cpp

all: starve rogue

starve:
	$(CC) $(CFLAGS) $(OUT_1).cpp $(SOURCE) -o $(OUT_1) -lpcap

rogue:
	$(CC) $(CFLAGS) $(OUT_2).cpp $(SOURCE) -o $(OUT_2) -lpcap
	
debug_starve: CFLAGS=-std=c++11 -Wall -g -O3 -DDEBUG_LOG_ENABLED -pedantic -pthread
debug_starve: starve

debug_rogue: CFLAGS=-std=c++11 -Wall -g -O3 -DDEBUG_LOG_ENABLED -pedantic -pthread
debug_rogue: rogue

clean:
	rm -f $(OUT_1) $(OUT_2) 
