/* 
 * File:   ethernet.h
 * Author: Lukas Cerny (xcerny63)
 */

#include <stdint.h>

#include <linux/if_ether.h>

#ifndef ETHERNET_H
#define ETHERNET_H

/**
 * Ziska pozici, na které zacina Ethernetova hlavicka
 * @return Pozice
 */
size_t getEthernetPrefix();
/**
 * Vrati velikost ethernetove hlavicky
 * @return Velikost
 */
size_t getEthernetHeaderSize();
/**
 * Vytvori ethernetovou hlavicku a provede jeji inicializaci
 * @param pakcet        Paket, kam se vytvori dana hlavicka
 * @param packetLen     Delka paketu
 * @param mac           Zdrojova MAC adresa
 */
void createEthernetHeader(uint8_t *packet, uint16_t *packetLen, uint8_t *mac);
/**
 * Vygeneruje MAC adresu
 * @return Vygenerovana MAC adresa
 */
uint8_t *generateMac();
/**
 * Vrati ukazatel na pozici v paketu, kde zacina ethernetova hlavicka
 * @param packet        Zdrojovy paket
 * @return              Ukazatel na hlavicku
 */
struct ethhdr *getEthernet(uint8_t *packet);
/**
 * Z prijatych dat vytvori paket
 * @param data      Prijata data
 * @param packet    Vytvoreny paket
 * @return          Delka paketu
 */
uint16_t getDataFromPacket(const u_char *data, uint8_t *packet);

#endif /* ETHERNET_H */
