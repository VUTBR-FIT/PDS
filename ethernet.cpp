/*
 * File:   ethernet.cpp
 * Author: Lukas Cerny (xcerny63)
 */

#include <cstring>

#include <arpa/inet.h>

#include "debug.h"
#include "ethernet.h"
#include "dhcp.h"

#ifndef BUFFER_SIZE
#define BUFFER_SIZE 4096
#endif

size_t getEthernetPrefix() {
    return 0;
}

size_t getEthernetHeaderSize() {
    return sizeof (struct ethhdr);
}

void createEthernetHeader(uint8_t *packet, uint16_t *packetLen, uint8_t *mac) {
    DEBUG_LOG("GEN_ETH", "Starting...");
    // Vytvoreni ethernetove hlavicky
    struct ethhdr *header = getEthernet(packet);
    // Nastaveni MAC adresy
    header->h_source[0] = mac[0];
    header->h_source[1] = mac[1];
    header->h_source[2] = mac[2];
    header->h_source[3] = mac[3];
    header->h_source[4] = mac[4];
    header->h_source[5] = mac[5];

    // Nastaveni cilove MAC adresy (broadcast)
    header->h_dest[0] = 0xFF;
    header->h_dest[1] = 0xFF;
    header->h_dest[2] = 0xFF;
    header->h_dest[3] = 0xFF;
    header->h_dest[4] = 0xFF;
    header->h_dest[5] = 0xFF;

    // Nataveni IP protokolu
    header->h_proto = htons(ETH_P_IP);
#ifdef DEBUG_LOG_ENABLED
    DEBUG_STAT_HEADER_SIZE("GEN_ETH", *packetLen, sizeof (struct ethhdr));
    DEBUG_LOG("GEN_ETH", "Finished");
#endif
}

struct ethhdr *getEthernet(uint8_t *packet) {
    return (struct ethhdr *) (packet + getEthernetPrefix());
}

uint16_t getDataFromPacket(const u_char *data, uint8_t *packet) {
    uint8_t *temp = (uint8_t *) const_cast<struct iphdr *> (reinterpret_cast<const struct iphdr *> (data));
    uint16_t size;
    for (size = 0; size < BUFFER_SIZE; size++)
        packet[size] = temp[size];

    return size;
}

uint8_t *generateMac() {
    uint8_t *mac = (uint8_t *) malloc(sizeof (uint8_t) * 6);
    uint8_t temp;
    for (int i = 0; i < 6; i++) {
        temp = (uint8_t) rand() % 256;
        if (i == 0)
            temp &= ~((uint8_t) 3);
        mac[i] = temp;
    }
    mac[0] = 0x00;
    mac[1] = 0x02;
    return mac;
}