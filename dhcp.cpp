/*
 * File:   dhcp.cpp
 * Author: Lukas Cerny (xcerny63)
 */

#include <cstring>
#include <stdlib.h>

#include "debug.h"
#include "udp.h"
#include "dhcp.h"

size_t getDHCPPrefix() {
    return getUDPPrefix() + getUDPHeaderSize();
}

size_t getDHCPDataSize() {
    return sizeof (struct dhcp_data);
}

void createDHCPData(uint8_t *packet, uint16_t *length, DHCP_OP op, uint8_t *mac, uint32_t xid) {
    DEBUG_LOG("GEN_DHCP", "Starting...");
    struct dhcp_data *dhcp = getDHCPData(packet);
    dhcp->op = op;
    dhcp->htype = 1; // 1= Ethernet 10Mbps
    dhcp->hlen = 6; // 6 for 10mb ethernet
    dhcp->hops = 0;
    dhcp->xid = xid;
    dhcp->secs = 0;
    dhcp->flags = 128; // 128=0b10000000= Broadcast
    dhcp->ciaddr = 0;
    dhcp->yiaddr = 0;
    dhcp->siaddr = 0;
    dhcp->giaddr = 0;
    dhcp->chaddr[0] = mac[0];
    dhcp->chaddr[1] = mac[1];
    dhcp->chaddr[2] = mac[2];
    dhcp->chaddr[3] = mac[3];
    dhcp->chaddr[4] = mac[4];
    dhcp->chaddr[5] = mac[5];
    // magic_cookie = 99.130.83.99
    dhcp->magic_cookie[0] = 0x63;
    dhcp->magic_cookie[1] = 0x82;
    dhcp->magic_cookie[2] = 0x53;
    dhcp->magic_cookie[3] = 0x63;

    *length = getDHCPPrefix() + getDHCPDataSize();
#ifdef DEBUG_LOG_ENABLED
    DEBUG_STAT_HEADER_SIZE("GEN_DHCP", *length, getDHCPDataSize());
    DEBUG_LOG("GEN_DHCP", "Finished");
#endif
}

void addDHCPOption(uint8_t *packet, uint16_t *packetLen, uint8_t optionType, uint8_t *optionValue, uint8_t optionSize) {
    DEBUG_PRINT("[%s]\t TYPE: %d, POSITION: %d, SIZE: %d\n", "DHCP_ADD_OPT", optionType, *packetLen, optionSize);
    packet[*packetLen] = optionType;
    *packetLen += sizeof (uint8_t);
    packet[*packetLen] = optionSize;
    *packetLen += sizeof (uint8_t);
    memcpy(&packet[*packetLen], optionValue, optionSize);
    *packetLen += optionSize;
    DEBUG_STAT_HEADER_SIZE("DHCP_ADD_OPT", *packetLen, (long unsigned int) optionSize + 2 * sizeof (uint8_t));
}

void addDHCPOptionClientID(uint8_t *packet, uint16_t *packetLen, uint8_t *mac) {
    DEBUG_PRINT("[%s]\t POSITION: %d\n", "DHCP_OPT_CLIENT_ID", *packetLen);
    packet[*packetLen] = DHCP_OPT_CLIENT_IDENTIFIER;
    *packetLen += sizeof (uint8_t);
    packet[*packetLen] = 7 * sizeof (uint8_t);
    *packetLen += sizeof (uint8_t);
    packet[*packetLen] = 0x01; // Pro MAC adresu
    packet[*packetLen += sizeof (uint8_t)] = mac[0];
    packet[*packetLen += sizeof (uint8_t)] = mac[1];
    packet[*packetLen += sizeof (uint8_t)] = mac[2];
    packet[*packetLen += sizeof (uint8_t)] = mac[3];
    packet[*packetLen += sizeof (uint8_t)] = mac[4];
    packet[*packetLen += sizeof (uint8_t)] = mac[5];
    *packetLen += sizeof (uint8_t);
    DEBUG_LOG("DHCP_ADD_CLIENT_ID", "END");
}

void addDHCPOptionDomainName(uint8_t *packet, uint16_t *packetLen, std::string domain) {
    DEBUG_PRINT("[%s]\t POSITION: %d\n", "DHCP_OPT_CLIENT_ID", *packetLen);
    packet[*packetLen] = DHCP_OPT_DOMAIN_NAME;
    *packetLen += sizeof (uint8_t);
    packet[*packetLen] = (uint8_t) domain.size();
    *packetLen += sizeof (uint8_t);
    memcpy(&packet[*packetLen], domain.c_str(), domain.size());
    *packetLen += (uint8_t) domain.size();
}

void addDHCPOptionEnd(uint8_t *packet, uint16_t *packetLen) {
    DEBUG_PRINT("[%s]\t POSITION: %d\n", "DHCP_OPT_END", *packetLen);
    uint8_t opt = 255;
    memcpy(&packet[*packetLen], &opt, sizeof (uint8_t));
    *packetLen += sizeof (uint8_t);

    if (*packetLen < DHCP_MIN_LENGTH)
        *packetLen += (DHCP_MIN_LENGTH - *packetLen) * sizeof (uint8_t);

    if (*packetLen % 2)
        *packetLen += sizeof (uint8_t);

}

struct dhcp_data *getDHCPData(uint8_t *packet) {
    return (struct dhcp_data *) (packet + getDHCPPrefix());
}

uint32_t getRandomXid() {
    return rand() % UINT32_MAX;
}

void getOption(uint8_t *packet, uint16_t packetLen, uint8_t optionType, uint8_t *value) {
    uint16_t temp = (getDHCPDataSize() + getDHCPPrefix());
    DEBUG_PRINT("[%s]\t START: %d MAX: %d\n", "DHCP_GET_OPT", temp, packetLen);

    for (uint16_t i = temp; i < packetLen && i < temp + 500; i++) {
        if (packet[i] != optionType) {
            i += packet[++i] + 1;
        } else {
            memset(value, 0, packet[i + 1]);
            for (int j = 0; j < packet[i + 1]; j++) {
                value[j] = packet[i + 2 + j];
            }
            break;
        }
    }
}
